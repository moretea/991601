class Result
  include Mongoid::Document
  include Mongoid::Timestamps

 FIELD_NAMES  = [:persoonlijk, :versie, :brainstorm, :brainstorm_time]
  (1..80).each { |n| FIELD_NAMES << :"stroop_#{n}" }
  (1..6).each  { |n| FIELD_NAMES << :"tekst_check_q#{n}" }
  (1..8).each  { |n| FIELD_NAMES << :"likert-l#{n}" }

  (1..9).each  { |n| FIELD_NAMES << :"likert-exit_b#{n}" }
  (1..9).each  { |n| FIELD_NAMES << :"likert-exit_t#{n}" }

  (1..8).each  { |n| FIELD_NAMES << :"exit_check#{n}" }

  (1..8).each  { |n| FIELD_NAMES << :"exit_check#{n}" }

  WHERES = FIELD_NAMES.map { |field_name| {field_name.exists => true}}.reduce({}) { |a, b| a.merge(b) }

  scope :completed, where(WHERES)


end
